package com.whileloop;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * Jan 29, 2021
 **/

public class WhileLoopSample {
	
	public static void main(String[] args) {
	
		int num = 1;
		
		System.out.println("Multiplication Table\n");
		while (num <= 10) {
			System.out.println("1 x " + num + " = " + (1*num) + "\t2 x " + num + " = " + (2*num)+ "\t3 x " + num + " = " + (3*num)+ "\t4 x " + num + " = " + (4*num)+ "\t5 x " + num + " = " + (5*num));
			num++;
		}
		
	}

}
